﻿namespace ShrekGame
{
    partial class FormMainGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMainGame));
            this.labelRound = new System.Windows.Forms.Label();
            this.labelLifes = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelBack = new System.Windows.Forms.Label();
            this.labelSave = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.progressBarShield = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBoxOgien = new System.Windows.Forms.PictureBox();
            this.pictureBoxDragon = new System.Windows.Forms.PictureBox();
            this.pictureBoxShield = new System.Windows.Forms.PictureBox();
            this.pictureBoxShrek = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOgien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDragon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShield)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShrek)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRound
            // 
            this.labelRound.AutoSize = true;
            this.labelRound.Location = new System.Drawing.Point(215, 17);
            this.labelRound.Name = "labelRound";
            this.labelRound.Size = new System.Drawing.Size(45, 13);
            this.labelRound.TabIndex = 2;
            this.labelRound.Text = "Round: ";
            // 
            // labelLifes
            // 
            this.labelLifes.AutoSize = true;
            this.labelLifes.Location = new System.Drawing.Point(306, 17);
            this.labelLifes.Name = "labelLifes";
            this.labelLifes.Size = new System.Drawing.Size(32, 13);
            this.labelLifes.TabIndex = 3;
            this.labelLifes.Text = "Lifes:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(174, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Round: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(268, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Lifes:";
            // 
            // labelBack
            // 
            this.labelBack.AutoSize = true;
            this.labelBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBack.Location = new System.Drawing.Point(-3, 2);
            this.labelBack.Name = "labelBack";
            this.labelBack.Size = new System.Drawing.Size(78, 32);
            this.labelBack.TabIndex = 8;
            this.labelBack.Text = "Back";
            this.labelBack.Click += new System.EventHandler(this.labelBack_Click);
            // 
            // labelSave
            // 
            this.labelSave.AutoSize = true;
            this.labelSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSave.Location = new System.Drawing.Point(81, 2);
            this.labelSave.Name = "labelSave";
            this.labelSave.Size = new System.Drawing.Size(80, 32);
            this.labelSave.TabIndex = 9;
            this.labelSave.Text = "Save";
            this.labelSave.Click += new System.EventHandler(this.labelSave_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(486, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "ShieldTime";
            // 
            // progressBarShield
            // 
            this.progressBarShield.Location = new System.Drawing.Point(552, 17);
            this.progressBarShield.Name = "progressBarShield";
            this.progressBarShield.Size = new System.Drawing.Size(188, 13);
            this.progressBarShield.TabIndex = 12;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBoxOgien
            // 
            this.pictureBoxOgien.Image = global::ShrekGame.Properties.Resources.Ogien;
            this.pictureBoxOgien.Location = new System.Drawing.Point(770, 170);
            this.pictureBoxOgien.Name = "pictureBoxOgien";
            this.pictureBoxOgien.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxOgien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOgien.TabIndex = 14;
            this.pictureBoxOgien.TabStop = false;
            // 
            // pictureBoxDragon
            // 
            this.pictureBoxDragon.Image = global::ShrekGame.Properties.Resources.Dragon;
            this.pictureBoxDragon.Location = new System.Drawing.Point(770, 130);
            this.pictureBoxDragon.Name = "pictureBoxDragon";
            this.pictureBoxDragon.Size = new System.Drawing.Size(200, 200);
            this.pictureBoxDragon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDragon.TabIndex = 13;
            this.pictureBoxDragon.TabStop = false;
            // 
            // pictureBoxShield
            // 
            this.pictureBoxShield.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxShield.Image")));
            this.pictureBoxShield.Location = new System.Drawing.Point(200, 150);
            this.pictureBoxShield.Name = "pictureBoxShield";
            this.pictureBoxShield.Size = new System.Drawing.Size(60, 70);
            this.pictureBoxShield.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxShield.TabIndex = 7;
            this.pictureBoxShield.TabStop = false;
            // 
            // pictureBoxShrek
            // 
            this.pictureBoxShrek.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxShrek.Image")));
            this.pictureBoxShrek.Location = new System.Drawing.Point(100, 200);
            this.pictureBoxShrek.Name = "pictureBoxShrek";
            this.pictureBoxShrek.Size = new System.Drawing.Size(70, 100);
            this.pictureBoxShrek.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxShrek.TabIndex = 6;
            this.pictureBoxShrek.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(356, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Score:";
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Location = new System.Drawing.Point(401, 17);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(0, 13);
            this.labelScore.TabIndex = 16;
            // 
            // FormMainGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 441);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBoxOgien);
            this.Controls.Add(this.pictureBoxDragon);
            this.Controls.Add(this.progressBarShield);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelSave);
            this.Controls.Add(this.labelBack);
            this.Controls.Add(this.pictureBoxShield);
            this.Controls.Add(this.pictureBoxShrek);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelLifes);
            this.Controls.Add(this.labelRound);
            this.Name = "FormMainGame";
            this.Text = "FormMainGame";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMainGame_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMainGame_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOgien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDragon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShield)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShrek)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelRound;
        private System.Windows.Forms.Label labelLifes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBoxShrek;
        private System.Windows.Forms.PictureBox pictureBoxShield;
        private System.Windows.Forms.Label labelBack;
        private System.Windows.Forms.Label labelSave;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar progressBarShield;
        private System.Windows.Forms.PictureBox pictureBoxDragon;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBoxOgien;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelScore;
    }
}