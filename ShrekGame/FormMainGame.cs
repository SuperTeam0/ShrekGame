﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

//TODO:Zabezpieczyć kulę ognia
namespace ShrekGame
{
    public partial class FormMainGame : Form
    {
        public int lifes;
        public int round;
        public int speed = 10;
        public int score = 0;
        public bool activeShield = false;
        public bool activeKula = false;
        public Thread tre;
        public Thread kula;
        public Thread end;

        public FormMainGame()
        {
            lifes = 3;
            round = 1;
            InitializeComponent();
            labelLifes.Text = lifes.ToString();
            labelRound.Text = round.ToString();
            labelScore.Text = score.ToString();
            ShieldPosition();
        }
        public FormMainGame(int ROUND,int LIFES,int SCORE)
        {
            lifes = LIFES;
            round = ROUND;
            score = SCORE;
            InitializeComponent();
            labelLifes.Text = lifes.ToString();
            labelRound.Text = round.ToString();
            labelScore.Text = score.ToString();
            ShieldPosition();
        }


       /* private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }*/

        /*private void buttonSave_Click(object sender, EventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(round);
            stringBuilder.Append("*");
            stringBuilder.Append(lifes);
            File.WriteAllText(@"Save", stringBuilder.ToString());
            MessageBox.Show("Zapisano grę.", "Save");
        }*/

        private void FormMainGame_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                if (pictureBoxShrek.Location.X + speed <= this.Width - 60)    pictureBoxShrek.Location = new Point(pictureBoxShrek.Location.X + speed, pictureBoxShrek.Location.Y);
            }
            if (e.KeyCode == Keys.Down)
            {
                if (pictureBoxShrek.Location.Y + speed <= this.Height - 70)  pictureBoxShrek.Location = new Point(pictureBoxShrek.Location.X , pictureBoxShrek.Location.Y+speed);
            }
            if (e.KeyCode == Keys.Left)
            {
                if (pictureBoxShrek.Location.X - speed >= 0)  pictureBoxShrek.Location = new Point(pictureBoxShrek.Location.X - speed, pictureBoxShrek.Location.Y);
            }
            if (e.KeyCode == Keys.Up)
            {
                if (pictureBoxShrek.Location.Y - speed >= 0) pictureBoxShrek.Location = new Point(pictureBoxShrek.Location.X, pictureBoxShrek.Location.Y-speed);
            }
            if (pictureBoxShrek.Location.X == pictureBoxShield.Location.X && pictureBoxShrek.Location.Y == pictureBoxShield.Location.Y)
            {
                if (pictureBoxShield.Visible)
                {
                    pictureBoxShield.Hide();
                    tre = new Thread(ShieldTime);
                    tre.Start();
                }
                
            }

        }

        private void ShieldTime()
        {
            activeShield = true;
            pictureBoxShrek.Image = System.Drawing.Image.FromFile( @"Images/ShrekArmor.jpg");
            for (int i = 0; i < 20; i++)
            {
                Thread.Sleep(500);
                Invoke(new Action(() => { progressBarShield.Value += 5; }));
            }
            System.Threading.Thread.Sleep(1000);
            pictureBoxShrek.Image = System.Drawing.Image.FromFile(@"Images/Shrek.jpg");
            activeShield = false;
            Invoke(new Action(() => { progressBarShield.Value = 0; }));
            Invoke(new Action(() => { pictureBoxShield.Show(); }));
            Random random = new Random();
            int x = random.Next(200, this.Width - 300);
            x -= x % 10;
            int y = random.Next(100, this.Height - 200);
            y -= y % 10;
            Point point = new Point(x, y);
            Invoke(new Action(() => { pictureBoxShield.Location = point; }));
        }

        private void labelBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void labelSave_Click(object sender, EventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(round);
            stringBuilder.Append("*");
            stringBuilder.Append(lifes);
            stringBuilder.Append("*");
            stringBuilder.Append(score);
            File.WriteAllText(@"Save", stringBuilder.ToString());
            MessageBox.Show("Zapisano grę.", "Save");
        }

        private void ShieldPosition()
        {
            Random random = new Random();
            int x = random.Next(200, this.Width - 300);
            x -= x % 10;
            int y = random.Next(100, this.Height - 200);
            y -= y % 10;
            Point point = new Point(x,y);
            pictureBoxShield.Location = point;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (activeKula)
            {
                if(pictureBoxOgien.Location.X>pictureBoxShrek.Location.X&& pictureBoxOgien.Location.X < pictureBoxShrek.Location.X + pictureBoxShrek.Width)
                {
                    if(pictureBoxOgien.Location.Y>pictureBoxShrek.Location.Y&& pictureBoxOgien.Location.Y < pictureBoxShrek.Location.Y + pictureBoxShrek.Height)
                    {
                        CheckIfEnd();
                    }
                    else if(pictureBoxOgien.Location.Y+pictureBoxOgien.Height > pictureBoxShrek.Location.Y && pictureBoxOgien.Location.Y + pictureBoxOgien.Height < pictureBoxShrek.Location.Y + pictureBoxShrek.Height)
                    {
                        CheckIfEnd();
                    }
                }
                if (pictureBoxOgien.Location.X+pictureBoxOgien.Width > pictureBoxShrek.Location.X && pictureBoxOgien.Location.X + pictureBoxOgien.Width < pictureBoxShrek.Location.X + pictureBoxShrek.Width)
                {
                    if (pictureBoxOgien.Location.Y > pictureBoxShrek.Location.Y && pictureBoxOgien.Location.Y < pictureBoxShrek.Location.Y + pictureBoxShrek.Height)
                    {
                        CheckIfEnd();
                    }
                    else if (pictureBoxOgien.Location.Y + pictureBoxOgien.Height > pictureBoxShrek.Location.Y && pictureBoxOgien.Location.Y + pictureBoxOgien.Height < pictureBoxShrek.Location.Y + pictureBoxShrek.Height)
                    {
                        CheckIfEnd();
                    }
                }
            }
            else
            {
                Point staryPoint = new Point(770, 170);
                pictureBoxOgien.Location = staryPoint;
                activeKula = true;
                kula = new Thread(Kula);
                kula.Start();
            }
        }

        private void Kula()
        {
            int x = 0;
            int yShreka = 0;
            int yKuli = 0;
            Invoke(new Action(() => { yShreka = pictureBoxShrek.Location.Y; yKuli = pictureBoxOgien.Location.Y; x = pictureBoxOgien.Location.X; }));
            int przemieszczenieY = yShreka - yKuli;
            przemieszczenieY /= 10;
            while (x > 100 && x < 780) 
            {
                Thread.Sleep(500);
                Invoke(new Action(() => { x = pictureBoxOgien.Location.X; yKuli = pictureBoxOgien.Location.Y; }));
                Point point = new Point(x-100, yKuli+przemieszczenieY);
                Invoke(new Action(() => { pictureBoxOgien.Location = point; }));
            }
            activeKula = false;

        }

        private void FormMainGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(activeShield==true) tre.Abort();
            kula.Abort();

        }

        private void CheckIfEnd()
        {
            kula.Abort();
            timer1.Stop();
            if (activeShield)
            {
                end = new Thread(End);
                end.Start();
            }
            else
            {
                MessageBox.Show("GameOver.", ":(");
                this.Close();
            }
        }

        private void End()
        {
            int x = 0, y = 0, x1 = 0, y1 = 0;
            Invoke(new Action(() => { x = pictureBoxOgien.Location.X; y = pictureBoxOgien.Location.Y; x1 = pictureBoxDragon.Location.X + (pictureBoxDragon.Width / 2); y1 = pictureBoxDragon.Location.Y + (pictureBoxDragon.Height / 2); }));
            int przesX, przesY;
            przesX = (x1 - x) / 10;
            przesY = (y1 - y) / 10;
            for(int i = 0; i < 10; i++)
            {
                Point wektor = new Point(przesX+x, przesY+y);
                Thread.Sleep(500);
                Invoke(new Action(() => { pictureBoxOgien.Location = wektor;x = pictureBoxOgien.Location.X;y = pictureBoxOgien.Location.Y; }));
            }
            MessageBox.Show("You Win!", ":)");
            score++;
            Invoke(new Action(() => { labelScore.Text = score.ToString(); }));
        }
    }
}
